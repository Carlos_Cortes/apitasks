# About the C# Taks API:
Description:
The API works with an SQLite relational database, which does not need a container to run, making it easy to use.
Contains 5 EndPoints:
### 1. [GET] /get-tasks
Which will provide us with all the tasks within the database.
![alt text](./ApiLab3/images/endpoint1.png)
### 2. [GET] /get-by-id-task/{id}
Exclusively returns a Task using its ID.
![alt text](./ApiLab3/images/endpoint2.png)
### 3. [POST] /create-task
Create a task with its title, description, creation date and end date.
![alt text](./ApiLab3/images/endpoint3.png)
### 4. [PUT] /update-by-id-task/{id}
Used to edit a record in the database.
### 5. [DELETE] /delete-by-id-task/{id}
Delete a task using its ID.
![alt text](./ApiLab3/images/endpoint5.png)

-------------------------------
# How to Run the API on Java C#
Requisites:
- Have dotnet version 8 or the most current installed on the PC.

## Execution form 1:
You must move to the root folder, you can do it with the following command:
```
cd ApiLab3/
```

Once in the API root directory, run the command:
```
dotnet run
```

You can view in the browser using the following URL:
```
http://localhost:5295/swagger/index.html
```
-------------------------------
## Execution form 2:
It can be run with a code editor (VisualStudio code with the C# Dev Kit extension is recommended)







