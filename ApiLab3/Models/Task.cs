using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApiLab3.Models
{
    public class Task
    {
        public long Id { get; set;}
        public string Title { get; set; }
        public string Description { get; set; } = string.Empty;
        public DateTime CreateDate { get; set; }
        public DateTime FinishDate { get; set; }
        public bool Status { get; set; }

        public Task(){}
    }
    
}