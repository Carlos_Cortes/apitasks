using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApiLab3.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ApiLab3.Controllers
{
    public class TaskController : ControllerBase
    {
        private readonly ILogger<TaskController> _logger;
        private readonly AppDataContext _context;
        //private Models.Task task;
       
        public TaskController(ILogger<TaskController> logger, AppDataContext context)
        {
            _logger = logger;
            _context = context;
        }
 
        [HttpGet("get-tasks")]
        public async Task<ActionResult<IEnumerable<ApiLab3.Models.Task>>> GetTasks()
        {
            return await _context.Tasks.ToListAsync();
        }
 
        [HttpGet("get-by-id-task/{id}")]
        public async Task<ActionResult<ApiLab3.Models.Task>> GetTask(long id)
        {
            var task = await _context.Tasks.FindAsync(id);
 
            if (task == null)
            {
                return NotFound();
            }
 
            return task;
        }

        [HttpPost("create-task")]
        public async Task<ActionResult<ApiLab3.Models.Task>> CreateTask(string title, string description, DateTime creationDate, DateTime finishDate) 
        {
            Models.Task task = new Models.Task();

            task.Title = title;
            task.Description = description;
            task.CreateDate = creationDate;
            task.Status = false;
            task.FinishDate = finishDate;
            
            _context.Tasks.Add(task);

            if (finishDate > creationDate)
            {
                await _context.SaveChangesAsync();
                    
                return CreatedAtAction(nameof(GetTask), new { id = task.Id }, task);
            }else
            {
                return BadRequest();
            }

        }

            [HttpPut("update-by-id-task/{id}")]
        public async Task<ActionResult> Put(int id, ApiLab3.Models.Task task)
        {
            if(id != task.Id)
            {
                return BadRequest();
            }
 
            _context.Entry(task).State = EntityState.Modified;
            await _context.SaveChangesAsync();
 
            return Ok();
        }

       
        [HttpDelete("delete-by-id-task/{id}")]
        public async Task<ActionResult<ApiLab3.Models.Task>> Delete(long id)
        {
            var task = await _context.Tasks.FindAsync(id);
            if (task == null)
            {
                return NotFound();
            }
 
            _context.Tasks.Remove(task);
            await _context.SaveChangesAsync();
 
            return task;
        }

        
    }
}