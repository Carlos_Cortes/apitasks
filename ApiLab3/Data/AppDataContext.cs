using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace ApiLab3.Models
{
    public class AppDataContext : DbContext
    {
        public AppDataContext(DbContextOptions<AppDataContext> options) : base(options){}
        public DbSet<ApiLab3.Models.Task> Tasks{ get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApiLab3.Models.Task>(builder =>
            {
                builder.HasKey(x => x.Id);
                builder.Property(x => x.Title).HasMaxLength(80);
                builder.Property(x => x.Description).HasMaxLength(250);
                builder.Property(x => x.CreateDate).HasColumnType("Date").IsRequired(true);
                builder.Property(x => x.FinishDate).HasColumnType("Date");
                builder.Property(x => x.Status).HasDefaultValue(false);
            });
        }
    }
}